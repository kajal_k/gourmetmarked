var gourmet = {

  init: function () {
    
    this.scrollFilter();
    this.mobileWrapperPopupClose();
    this.mobilemenuopen();
    this.slotSelector();
    this.activeMenu();
    this.accordion();     
    this.openVendorVideo();
    this.closeVideo();
    this.cartDrawerOpen();
    this.cartDrawerClose();
    this.search();
    this.qtyModifier();
    this.slickCall(); 
    this.addToCartNew();
    this.cartDrawer();
    this.removeCartAjax();
    this.cartJsCount();
    this.deliveryDate();
    this.randomProduct();
    this.checkSelectBox();
    this.simpleVariant();
    this.chargesFee();
    this.changeQTY();
    this.carttoproqty();
    this.hidemenucontainer();
    this.drawerQtyChange();
    this.radioVarChange();
    this.onLoadSelect();
  },

  onLoadSelect:function(){
    $('.product-form, .collectionListIndividual .product-form, .results_search .product-form').each(function(){
      var form = $(this);
      var callBackVariantId = form.attr('data-variant-id');
      var CartItemQuantity = 0;
      $.ajax({
        type: 'GET',
        url: '/cart.js',        
        dataType: 'json',
        success: function(data){
          $(data.items).each(function(index,value){
            var cart_id = value.variant_id;
            if( cart_id == callBackVariantId){
              CartItemQuantity = value.quantity;
              form.find('.item-in-cart').show();
              form.find('.product-block').addClass('product-added');
              form.find(".cart-text").text('Tilføj');
              form.find('.total-in-cart').text(CartItemQuantity);
              form.find('input[name="quantity"]').val(CartItemQuantity);
            }
          });                               
        }
      });
    });
  },
  
  radioVarChange:function(){
    $('.product-form .variant-radio').trigger('click');    
    $("body").on('click','.variant-radio',function(){
      var $this = $(this);
      var getId;
      var inpVal = $this.find('input').val();
      var optVal; 
      $this.find('input').prop('checked',true);
      var variant_id = $this.attr('data-id');
      $this.closest('form').attr('data-first_variant',variant_id);
      $this.closest('form').find('.custom_select option').each(function(){
        var $data = $(this);
        optVal = $data.val();
        if(inpVal === optVal){
          $data.closest('form').find('.custom_select').val(optVal).trigger('change');
          var price = $data.attr('data-price');
          $data.closest('form').find('.product-price').text(price);
        }
      });                            

      var selected_id = $this.attr('data-id');
      var CartItemQuantity = 0;
      $.ajax({
        type: 'GET',
        url: '/cart.js',        
        dataType: 'json',
        success: function(data){
          $(data.items).each(function(index,value){
            var cart_id = value.variant_id;
            if( cart_id == selected_id){
              CartItemQuantity = value.quantity;
              $this.closest('form').find('.item-in-cart').show();
              $this.closest('form').find('.product-block').addClass('product-added');
              $this.closest('form').find('.total-in-cart').html(CartItemQuantity);
              $this.closest('form').find('.product-block .qty').val(CartItemQuantity);
              $this.closest('form').find('.cart-text').text('Tilføj');
              return false;
            }else{
              CartItemQuantity = 1;
              $this.closest('form').find('.cart-text').text('Tilføj til kurven');
              $this.closest('form').find('.product-block').removeClass('product-added');
              $this.closest('form').find('.product-block .qty').val(CartItemQuantity);
            }
          })
        }
      });
    });
  },
  
  drawerQtyChange:function(){ 
    $(document).keypress(function(e) {
      if(e.which == 13) {
        e.preventDefault();
        var product_id = $(e.target).attr('data-item');
        var qty_val = $(e.target).val();
        $.ajax({
          type: 'GET',
          url: '/cart/change.js',
          dataType: 'json',
          data: {id: product_id, quantity: qty_val },
          success: function(response) {
            var money = $('.money_filter').text();
            for( var i = 0; i<response.items.length; i++){
              if(response.items[i].id == product_id){
                $('#pr_money_'+product_id).text(Shopify.formatMoney(response.items[i].original_line_price, money));
                $('.'+response.items[i].handle).find('.total-in-cart').text(response.items[i].quantity);
                $('.'+response.items[i].handle).find('.product-quantity').val(response.items[i].quantity);
              }
            }
            gourmet.cartDrawer();
            gourmet.cartJsCount();
          }
        });
      }
    });
  },
  
  getAjaxCart:function(){
    $.ajax({
      type: 'GET',
      url: '/cart.js',
      dataType: 'json',
      success: function(data) { 
        for(var i = 0; i < data.item_count; i++){
          if(data.items[i]){
            var prouct_id = data.items[i].product_id;
            var title = data.items[i].title;              
            var cartQty = data.items[i].quantity;
            var variant_id = data.items[i].id;
            $("#product-select-"+prouct_id+"> option[value=" + variant_id + "]").attr('data-qty',cartQty);              
            var selectedOption = $("#product-select-"+prouct_id+"> option:selected");
            var selectedOptionQty = $("#product-select-"+prouct_id+"> option:selected").attr('data-qty');
            if(selectedOptionQty > 0){
              selectedOption.parents('.product-block').addClass('product-added');
              selectedOption.parents('.product-block').find('.total-in-cart').text(selectedOptionQty);
              selectedOption.parents('.product-block').find('.qty').val(selectedOptionQty);
            }

            $('#basket_img_'+variant_id).show();
            $("#basket_"+variant_id).text(data.items[i].quantity);
          }
        }
      }
    }); 
  },
  
  hidemenucontainer:function(){
    var widthW = $(window).width();
    if(widthW < 960 ){
      $('.product-menu-container').remove();
    }
  },
  
  carttoproqty : function(){
    $('body').on('click','.qty-button',function(){
      var updated_value = $(this).siblings('.focus_event').val();
      var updated_pro = $(this).siblings('.focus_event').attr('data-find');
      $('#basket_img_'+updated_pro).find('.total-in-cart').text(updated_value);
    });        
  },
  
  changeQTY : function(){
    $('body').on('click','.variant-minus',function(e){
      e.preventDefault();
      var $this = $(this);
      var handle = $this.attr('data-handle');
      var id = $this.closest('form').find('input[name="id"],select[name="id"]').val();
      var oldValue = $this.siblings(".qty").val();            
      if (oldValue > 0) {
        var newVal = parseFloat(oldValue) - 1;
      } else {
        newVal = 0;
      }
      if(newVal == 0){
        $this.siblings(".qty").val(0);
        var qty_val = newVal;
      }else{
        $this.siblings(".qty").val(newVal);
        var qty_val = $this.siblings(".qty").val();
      }
      $.ajax({
        type: 'GET',
        url: '/cart/change.js',
        dataType: 'json',
        data: {id: id, quantity: qty_val},
        success: function(response) {

          gourmet.cartDrawer();
          gourmet.cartJsCount();
          var val = $this.closest('form').find('.qty').val();
          $this.closest('form').find('.total-in-cart').text(val);
          $('.'+handle).find('.total-in-cart').text(val);
          $('.'+handle).find('.product-quantity').val(val);
          if(val > 0){
            val = parseInt(val) - parseInt(1);
          }else{
            $this.closest('form').find('.item-in-cart').hide();
            $this.closest('form').find('.product-block').removeClass('product-added');
            $('.'+handle).removeClass('product-added');
            $this.closest('form').find('.cart-text').text('Tilføj til kurven');
            $this.closest('form').find(".qty").val(1);
            $('.'+handle).find(".qty").val(1);
          }
        }
      });
    });
  },
  
  simpleVariant : function(){    
    $('.variant-wrapper input').click(function(){      
      var val = $(this).val();
      var varid = $(this).closest('form').find('item-in-cart').text();
      if(varid != 0){
        $(this).closest('form').find('item-in-cart').show().addClass('active');
      }
    });
  },
  
  chargesFee : function(){
    $.ajax({
      type: 'GET',
      url: '/cart.js',
      dataType: 'json',
      success: function(data) {
        if(data.item_count < 2){
//           if(data.items[0].id == 3273730228245){
//             $("a#checkout_btn").css("pointer-events","none");
//           }else{
//             $("a#checkout_btn").css("pointer-events","visible");
//           }
        }
      }
    });
  },
    
  checkSelectBox : function(){
    var open = false;
    function isOpen(x){
      if(open){       
        x.addClass('open');
      }
      else{
        x.removeClass('open');
      }
    }
    $(".single-option-selector").on("click", function() {
      var selectElement = $(this);
      open = !open;
      isOpen(selectElement);
    });
    $(".single-option-selector").on("blur", function() {
      var selectElement = $(this);
      if(open){
        open = !open;
        isOpen(selectElement);
      }
    });
    $(document).keyup(function(e) {
      var selectElement = $('select');
      if (e.keyCode == 27) { 
        if(open){
          open = !open;
          isOpen();
        }
      }
    });
  },
  
  randomProduct: function(){

  },
    
  deliveryDate: function(){
    var tm =new Date();
    var setTime = '10.00-15.00';
    var time = tm.getHours() + ":" + tm.getMinutes();
    var date = new Date();
    var days = ["SØN","MAN","TIRS","ONS","TORS","FRE","LØR"];
    var months = ['JAN','FEB','MAR','APR','MAJ','JUNI','JULI','AUG','SEPT','OKT','NOV','DEC'];
    var thisMonth = months[date.getMonth()];
    var spc = " "; 
    var todate , toDay ,nextdate,nextday , nextDay , count = 1 , zipdata , appDate, appMonth , timeOnClick , appDay;
    var array = [];
    var check_cookie = $.cookie("cookie_set");
    var set_cookie_date = $.cookie("cookie_set_date");
    var set_cookie_time = $.cookie("cookie_set_time");
    var set_cookie_index = $.cookie("set_cookie_index");
    $(".form-zipcode input[type='text']").keyup(function(event) {
      if (event.keyCode === 13) {
        $('.form-zipcode').submit();
      }
    });
    $('.form-zipcode').submit(function(e){
      e.preventDefault();
      zipdata = $(this).find('.temp').html();
      array = zipdata.split(',');
      var value = $(this).find('input[type="text"]').val();
      if(array.indexOf(value) > -1){
        $('.unsupported-zip').hide();
        $(this).closest('.drawer-zip-verification').addClass('zip-verified').removeClass('class-here');
        $.cookie('cookie_set', value, { expires: 1 });
        setTimeout(function(){
          $('.delivery-drawer-switch').addClass('close_verified').removeClass('switch-delivery');
          $('.switch-delivery').off("click");
          $('.delivery-drawer').removeClass('open');
          $('.calendar-drawer').addClass('open');
          $('.delivery-overlay').fadeOut();
          $('.calendar-drawer-overlay').fadeIn();
          $('#delivery-container').addClass('zip-verified');
          $('.mobile-zip-deails').addClass('zip-verified').removeClass('class-here');
        },1500);
      }
      else
      {
        $('.unsupported-zip').show();
      }
    });      

    $('body').on('click','.close_verified',function(){      
      $('.calendar-drawer').toggleClass('open');
      $('body,html').toggleClass('delivery-overflow');
      $('.calendar-drawer-overlay').fadeToggle();
    });

    if(check_cookie != undefined){
      $('.delivery-drawer-switch').removeClass('switch-delivery');
      $('.delivery-drawer-switch').addClass('close_verified');
      $('.switch-delivery').off("click");
      $('#delivery-container').addClass('zip-verified');
      $('.mobile-zip-deails').addClass('zip-verified').removeClass('class-here');
    }

    $('body').on('click','.switch-delivery',function(){      
      $('body,html').toggleClass('delivery-overflow');
      $('.delivery-overlay').fadeToggle();
      $('.delivery-drawer').toggleClass('open');
    });

    if( parseInt(time) < parseInt(6) )
    {
      todate = date.getDate();
      toDay  = days[date.getDay()] +" "+  todate +" "+  thisMonth;      
      $('.site-actions-cart-label #time_head').text(setTime);
      $('.site-actions-cart-label #date_head').text(toDay);
      $('.note-cart .cart-note-content').text("Date : " + toDay + "\n" + "Time : " + setTime);
    }
    else
    {
      var nextday = new Date();
      nextday.setDate(date.getDate()+1);
      nextDay = days[nextday.getDay()] +" "+ nextday.getDate() + " " +  months[nextday.getMonth()];
      $('.site-actions-cart-label #time_head').text(setTime);
      $('.site-actions-cart-label #date_head').text(nextDay);
      $('.note-cart .cart-note-content').text("Date : " + nextDay + "\n" + "Time : " + setTime);
      $('.calendar-body .calendar-slider .calendar-for .slick-track .slick-slide').each(function(){
        var attr = $(this).attr('data-slick-index');
        if(attr == 0){
          var slideDate = $(this).find('.date').text().split('/');
          if(parseInt(slideDate) != nextdate){
            $('.calendar-body .calendar-slider .thumb-slider .slick-track .slick-slide').each(function(){
              var attr = $(this).attr('data-slick-index');
              if(attr == 0){
                $(this).addClass("disabled");
              }
              else{
                return false;
              }
            });
          }
        }else{
          return false;
        }
      });
    }

    $('body').on('click','.individual-slot',function(){
      timeOnClick = $(this).attr('data-dt');
      $('.site-actions-cart-label #time_head').text(timeOnClick);
      var data_index = $(this).closest('.slick-slide').attr('data-slick-index');
      $('.calendar-for .slick-track .slick-slide').each(function(){
        var cal_data_index = $(this).attr('data-slick-index');
        set_cookie_date = $.cookie("cookie_set_date");
        set_cookie_time = $.cookie("cookie_set_time");
        if(data_index == cal_data_index){
          $.cookie('set_cookie_index', cal_data_index , { expires: 1 });
          appDate = $(this).find('.date').text().split('/')[0];
          appMonth = $(this).find('.date').text().split('/').pop();
          appDay = $(this).find('.day').text();
          toDay  = appDay + spc + appDate + spc + months[appMonth-1];
          $('.site-actions-cart-label #date_head').text(toDay);
          $('.note-cart .cart-note-content').text("Date : " + toDay + "\n" + "Time : " + timeOnClick);
          $.cookie('cookie_set_date', toDay , { expires: 1 });
          $.cookie('cookie_set_time', timeOnClick, { expires: 1 });          	
          $('#time-date-mobile, #time-date-desk').text(toDay + ', kl.' + timeOnClick);
        }
      });     
    });

    if(set_cookie_date != undefined && set_cookie_time != undefined) 
    {
      $('.calendar-nav .slick-slide[data-slick-index="'+ set_cookie_index +'"]').find('.individual-slot[data-dt="'+ set_cookie_time +'"]').addClass('selected');
      $('.delivery-drawer').removeClass('open');
      $('.site-actions-cart-label #time_head').text(set_cookie_time);
      $('.site-actions-cart-label #date_head').text(set_cookie_date);
      $('.note-cart .cart-note-content').text("Date : " + set_cookie_date + "\n" + "Time : " + set_cookie_time);
      $('#time-date-mobile, #time-date-desk').text(set_cookie_date + ', kl.' + set_cookie_time);
    }else{
      $('.calendar-nav .slick-slide[data-slick-index="1"]').find('.individual-slot[data-dt="10.00-15.00"]').addClass('selected');
    }
    $('#open_drawer_date,#open_drawer_date_mob').click(function(){
      $('.delivery-drawer-switch').trigger('click');
    });
  },    
  
  removeCartAjax: function(){
    var qty;
    $('.basket-items').each(function(){
      var item = $(this).data('item');
      $('.removeCartAjax_'+item).click(function(e){
        var $this = $(this);
        var id = $(this).data('id');
        var pro = $(this).attr('data-pro-id');
        qty = $('#quantity_'+item).attr('value');        
        $.ajax({
          type: 'POST',
          url: '/cart/change.js',
          data: {id: id, quantity: 0} ,
          dataType: 'json',
          beforeSend:  function(){
            $this.closest('.basket-items').find('.remove-item-overlay').fadeIn();
          },
          success: function(data) {
            $.ajax({
              url:'/cart',
              type:"GET",
              dataType:"html",
              success:function(data){
                $('.'+pro).find('.total-in-cart').text('0');
                $('.'+pro).find('.product-quantity').val(1);
                $('.'+pro).removeClass('product-added');
                $('.'+pro).find('.cart-text').text("Tilføj til kurven");
                var cartData = $(data).find('.basket_bucket').html();
                $('.basket_bucket').html(cartData);                
                $('.loading').hide();                
                gourmet.removeCartAjax();
                gourmet.cartDrawer();
                gourmet.cartJsCount();
                gourmet.chargesFee();
              }
            });
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log('error');
          }
        });
      });
    });
  },
  
  cartJsCount : function() {
    var id,flag=0,deliveryPrice;
    var money = $('.money_filter').text();
    $.ajax({
      type: 'GET',
      url: '/cart.js',
      dataType: 'json',
      success: function(response) {
        if(response.items.length > 0){
          for(var i = 0 ; i < response.items.length ; i++){
            if(response.items[i].variant_id != null){
              id = response.items[i].variant_id;
              if(id == 3273730228245){
                deliveryPrice = response.items[i].price;
                flag = 1;
                $.ajax({
                  type: 'GET',
                  url: '/cart/change.js',
                  dataType: 'json',
                  data: {id: id, quantity: 1},
                  success: function(response) {
                  }
                });
              }
            }
          }
        }
        if(deliveryPrice == undefined){
          var mimusTotle = response.total_price;
          $('.price-goods p span').text(Shopify.formatMoney(mimusTotle, money));
          $('.price-delivery p span').text(Shopify.formatMoney(0, money));
        }else{
          var mimusTotle = parseInt(response.total_price) - parseInt(deliveryPrice);
          $('.price-goods p span').text(Shopify.formatMoney(mimusTotle, money));
          $('.price-delivery p span').text(Shopify.formatMoney(deliveryPrice, money));
        }
        if(flag == 1){
           $('.cart-count').text(response.item_count-1);
        }else{
          $('.cart-count').html(response.item_count);
        }
        var checkText = $('.cart-count').text();
        checkText = parseInt(checkText);
        if(checkText == 1 ){
          $('.drawer__header .cartTitle').text('VARE I KURVEN');
        }else{
          $('.drawer__header .cartTitle').text('VARER I KURVEN');
        }
        if(response.total_price > 60000){
          $('.calendar-header .sub-title').text('Levering fra 0 kr.');
        }else{
          $('.calendar-header .sub-title').text('Bestillinger under 600 kr tilføjes 60 kr i grundgebyr.');
        }
        var price = Shopify.formatMoney(response.total_price, money);
        $('.site-actions-cart-label .head_cart').html(price);
        $('.fixed-menu-item .mobile-cart-price').html(price);
        $('.site-actions-cart-label .head_cart').attr('data-price',response.total_price);
      }
    });   
  },

  cartDrawer : function() {
    $.ajax({
      type: 'GET',
      url: '/cart',
      dataType: 'html',
      success: function(response) {
        var product = $(response).find('.basket_bucket').html();
        $('.cart-drawer-listing').html(product);
        var total = $(response).find('#fetch-val').html();
        $('#fetch-val').html(total);
        $('#delivery-fetch').html(total);
        $('#delivery-fetch-cal').html(total);
        gourmet.removeCartAjax();
      }
    });      
  },
  
  
  addToCartNew : function() {
    $(document).keypress(function(e) {
      if(e.which == 13) {
        e.preventDefault();
        var qty = $(e.target).closest('.product-block').find('.product-quantity').val();
        var id = $(e.target).closest('.product-block').find('input[name="id"],select[name="id"]').val();
        var handle = $(e.target).closest('.product-block').find('.cart-text').attr('data-handle');
        var $response = "id="+id+"&quantity="+qty;
        $.ajax({
          type: 'POST',
          url: '/cart/change.js',
          dataType: 'json',
          data: {id: id, quantity: 0},
          success:  function(){
            $.ajax({
              type: 'POST',
              url: '/cart/add.js',
              dataType: 'json',
              data: $response,
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                return false;
              },
              beforeSend:  function(){
                $(e.target).text('Tilføjer');
              },
              success: function(response) {
                var variant_id = response.id;
                $('.'+handle).find('.item-in-cart').show();
                $('.'+handle).find('.total-in-cart').text(response.quantity);
                $('.'+handle).find('.product-quantity').val(response.quantity);
                $('.'+handle).addClass('product-added');
                $(e.target).text('Tilføj');
                $(e.target).closest('.product-block').find('.item-in-cart').show();
                $(e.target).closest('.product-block').addClass('product-added');
                $(e.target).closest('.product-block').find('.total-in-cart').text(response.quantity);
                $(e.target).closest('.product-block').find('.product-quantity').val(response.quantity);
                $(e.target).closest('.product-block').find('.cart-text').text('Tilføj');
                gourmet.removeCartAjax();
                gourmet.cartDrawer();
                gourmet.cartJsCount();
                gourmet.chargesFee();
              }
            });
          }
        });
      }
    });

    $('body').on('click','.cart-text',function(e){
      e.preventDefault();
      var $this = $(this);
      var closestForm = $this.closest('form');
      var id = closestForm.find('input[name="id"],select[name="id"]').val();
      var handle = $this.attr('data-handle');
      if(e.which == 13){

      }else{
        var qty = 1;
        var $response = "id="+id+"&quantity="+qty;
        $.ajax({
          type: 'POST',
          url: '/cart/add.js',
          dataType: 'json',
          data: $response,
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            return false;
          },
          beforeSend:  function(){
            $this.text('Tilføjer');
          },
          success: function(response) {
            var variant_id = response.id;
            $this.text('Tilføj');
            $('.'+handle).find('.item-in-cart').show();
            $('.'+handle).find('.total-in-cart').text(response.quantity);
            $('.'+handle).find('.product-quantity').val(response.quantity);
            $('.'+handle).addClass('product-added');
            $this.parents('.product-block').find('.item-in-cart').show();
            $this.parents('.product-block').addClass('product-added');
            $this.parents('.product-block').find('.total-in-cart').text(response.quantity);
            $this.parents('.product-block').find('.product-quantity').val(response.quantity);
            gourmet.removeCartAjax();
            gourmet.cartDrawer();
            gourmet.cartJsCount();
            gourmet.chargesFee();
          }
        });
      }
    });
  },

  scrollFilter : function(){
    var sections = $('.individual-faq')
    , nav = $('nav.filter-menu')
    , nav_height = $('.site-header').outerHeight();

    $(window).on('resize scroll', function () {
      var cur_pos = $(this).scrollTop();
      var offmargin;
      if($(window).width() > 767){
        offmargin = 35;
      }
      else{
        offmargin = 0;
      }
      sections.each(function () {
        var top = $(this).offset().top - nav_height - offmargin,
            bottom = top + $(this).outerHeight();        
        if (cur_pos >= top && cur_pos <= bottom) {
          nav.find('a').removeClass('active');
          sections.removeClass('active');

          $(this).addClass('active');
          nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');

          if($('.sub-menu a').hasClass('active')){
            var activeLink = $('.sub-menu a.active');
            $(activeLink).parents('.sub-menu').slideDown();
          }
          else{
            $('.sub-menu').slideUp();
          }
          nav.find('a[class*="active"]').parents('.sub-menu').siblings('a').addClass('active');
        }
      });
    });
    nav.find('a[class*=scroll-link]').on('click', function () {
      var $el = $(this)
      , id = $el.attr('href');      
      var offmargin;
      if($(window).width() > 767){
        offmargin = 35;
      }
      else{
        offmargin = 0;        
      }
      $('html, body').animate({
        scrollTop: $(id).offset().top + 10 - nav_height - offmargin
      }, 500);
      return false;
    });

    if($(window).width() > 767 && $('.faq-section').length > 0){      
      var distance = $('.faq-section').offset().top,          
      $window = $(window);
      $window.scroll(function () {
        if ($window.scrollTop() >= (distance - 135)) {
          $('.right-filter-menu').addClass('sticked');
        }
        else {
          $('.right-filter-menu').removeClass('sticked');
        }
      });
    }    
    var collectionheight = $('.collection-filter').outerHeight();
    var footerHeight = $('.footer-section').outerHeight();

    if(collectionheight > footerHeight){
      $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
      };

      $(window).on('resize scroll', function() {           
        $('.footer-section').each(function() {

          if ($(this).isInViewport()) {
            $('.collection-filter').addClass('more');
          } else {
            $('.collection-filter').removeClass('more');
          }
        });
      });      
    }
  },
  
  mobileWrapperPopupClose : function(){
    $('.mobile-wrapper-close').click(function(){
      $(this).parents('.mobile-wrapper-popup').removeClass('visible open');
      $('body,html').removeClass('overflow-hidden');
      $('body,html').removeClass('cart-overflow');
      $('body,html').removeClass('delivery-overflow');
      $('.fullscreen-overlay').fadeOut();
    });
  },
  
  mobilemenuopen : function(){
    $('.mobile-menu-item').click(function(){
      $('.mobile-footer-menu').toggleClass('open');
      $(this).toggleClass('active');
    });
  },   
  
  slotSelector:function(){
    $('.individual-slot').click(function () {
      $('.individual-slot').removeClass('selected');
      $(this).addClass('selected');
    });           
  },
  
  activeMenu : function(){
    if($(window).outerWidth() < 768){

      function scrollToActive(){
        var obj = $('.navmenu-link.active');
        var childPos = obj.offset();
        var parentPos = obj.parents('.navmenu').offset();
        var childOffset = {
          top: childPos.top - parentPos.top,
          left: childPos.left - parentPos.left
        }
        var currentScroll = $(".navmenu").scrollLeft();
        $(".navmenu").animate({
          scrollLeft: currentScroll + childOffset.left - 10
        });
      }
      var obj = $('.navmenu-link.active');       
      if(obj.length > 0){        
        scrollToActive();
      }     
      $('.navmenu-item').click(function(){
        $(this).siblings().children('.navmenu-link').removeClass('active');
        $(this).children('.navmenu-link').addClass('active');
        scrollToActive();        
      });
    }    
  },
  
  accordion : function(){
    $('.accordion-title').click(function () {
      $(this).parents('.accordion-wrapper').siblings().children('.accordion-content').slideUp();
      $(this).siblings('.accordion-content').slideToggle();
    });
  },
  
  openVendorVideo : function(){
    $('.video-link').click(function(){
      $('.vendor-video-wrapper').fadeIn();
    });
  },
  
  closeVideo : function(){
    $('.close-video').click(function(){
      $('.vendor-video-wrapper').fadeOut();
    });
  },
  
  cartDrawerOpen : function(){
    $('#AddToCart,.cart-menu-item').click(function () {
      $('.cart-drawer').addClass('open');
      $('body,html').addClass('cart-overflow');
      $('.cart-drawer-overlay').fadeToggle();
    });
    $('.cart-switch').click(function () {       
      $('.cart-drawer').toggleClass('open');
      $('body,html').toggleClass('cart-overflow');
      $('.cart-drawer-overlay').fadeToggle();
    });
  },
  
  cartDrawerClose : function(){
    $('.fullscreen-overlay').click(function () {
      $('.cart-drawer').removeClass('open');
      $('.delivery-drawer').removeClass('open');
      $('.calendar-drawer').removeClass('open');
      $('body,html').removeClass('overflow-hidden');
      $('.fullscreen-overlay').fadeOut();

    });
  },  

  search : function(){
    
    $('.search-menu-item').click(function(){
      $('.search-result-container').addClass('visible');
    }); 
    $('.close-search-result').click(function(){
      $('.search-itself').val('');
      $('.search-result-container').removeClass('visible');
      $('body,html').removeClass('overflow-hidden');
    }); 
    $('.close-search').click(function(){
      $('.search-itself').val('');
    });
  },
  
  qtyModifier : function(){
    var product_id;
    $("body").on("click",".dec-qty", function () { 
      var data_id = $(this).closest('.basket-items').attr('data-item');
      var $button = $(this);
      var oldValue = $button.siblings(".qty").val();
      product_id = $(this).siblings('.qty').data('item');
      if (oldValue > 2) {
        var newVal = parseFloat(oldValue) - 1;
      } else {
        newVal = 1;
      }
      $button.siblings(".qty").val(newVal);
      var qty_val = $button.siblings(".qty").val();
      $.ajax({
        type: 'GET',
        url: '/cart/change.js',
        dataType: 'json',
        data: {id: product_id, quantity: qty_val},
        success: function(response) {
          var money = $('.money_filter').text();
          for( var i = 0; i<response.items.length; i++){
            if(response.items[i].id == product_id){
              $('#pr_money_'+product_id).text(Shopify.formatMoney(response.items[i].original_line_price, money));
              $('.'+response.items[i].handle).find('.total-in-cart').text(response.items[i].quantity);
              $('.'+response.items[i].handle).find('.product-quantity').val(response.items[i].quantity);
            }
          }
          var price = Shopify.formatMoney(response.total_price, money);
          $('.site-actions-cart-label .head_cart').html(price);
          $('.fixed-menu-item .mobile-cart-price').html(price);
          $('.site-actions-cart-label .head_cart').attr('data-price',response.total_price);
          $('.checkout-action .justify-between #fetch-val .title').html(price);
          $('.delivery-footer .checkout-action .justify-between #fetch-val .title').html(price);
          gourmet.cartDrawer();
          gourmet.cartJsCount();
        }
      });
    });

    $("body").on("click",".inc-qty", function () {   
      var data_id = $(this).closest('.basket-items').attr('data-item');
      product_id = $(this).siblings('.qty').data('item');
      var $button = $(this);
      var oldValue = $button.siblings(".qty").val();
      var newVal = parseFloat(oldValue) + 1;
      $button.siblings(".qty").val(newVal);
      var qty_val = $button.siblings(".qty").val();
      $.ajax({
        type: 'GET',
        url: '/cart/change.js',
        dataType: 'json',
        data: {id: product_id, quantity: qty_val},
        success: function(response) {
          var money = $('.money_filter').text();
          for( var i = 0; i<response.items.length; i++){
            if(response.items[i].id == product_id){
              $('#pr_money_'+product_id).text(Shopify.formatMoney(response.items[i].original_line_price, money));
              $('.'+response.items[i].handle).find('.total-in-cart').text(response.items[i].quantity);
              $('.'+response.items[i].handle).find('.product-quantity').val(response.items[i].quantity);
            }
          }
          var price = Shopify.formatMoney(response.total_price, money);
          $('.site-actions-cart-label .head_cart').html(price);
          $('.fixed-menu-item .mobile-cart-price').html(price);
          $('.site-actions-cart-label .head_cart').attr('data-price',response.total_price);
          $('.checkout-action .justify-between #fetch-val .title').html(price);
          $('.delivery-footer .checkout-action .justify-between #fetch-val .title').html(price);
          gourmet.cartDrawer();
          gourmet.cartJsCount();
        }
      });
    });
  },
  
  slickCall : function(){
    var preventClick = false;
    $('.slick-link').click(function (e) {          
      if ($(this).parents('.banner-container').hasClass('slick-current')) {
        preventClick = true;
      }
      else {
        e.preventDefault();
      }
    });
    $(".banner").slick({
      dots: false,
      arrows:false,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 5000,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      focusOnSelect: true,
      lazyLoad: 'ondemand',
      //cssEase: 'cubic-bezier(0.600, -0.280, 0.735, 0.045)'
      responsive: [                    
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
      ]
    });
   
    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1                       
          }
        },
      ]
    });
    
    $('.slider-nav').slick({
      slidesToShow: 5,
      arrows: false,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 5,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
          }
        },
      ]
     });                        

        for (var i = 0; i < 10; i++) {
        var desiredDay = Date.today().add(i).days().getDayName();
        switch (desiredDay) { 
        case 'Monday': 
        desiredDay = 'Man';
        break;
        case 'Tuesday': 
        desiredDay = 'Tirs';
        break;
        case 'Wednesday': 
        desiredDay = 'Ons';
        break;
        case 'Thursday': 
        desiredDay = 'Tors';
        break;
        case 'Friday': 
        desiredDay = 'Fre';
        break;
        case 'Saturday': 
        desiredDay = 'Lør';
        break;
        case 'Sunday': 
        desiredDay = 'Søn';
        break;
        default:
        desiredDay = '';
        }
        var desiredDate = Date.today().add(i).days().toString(' d/M');
        $('.calendar-for').append('<div><p class="day text-center">' + desiredDay + '</p><p class="date text-center">' + desiredDate + '</p></div>');
        }

        $('.calendar-for').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        swipe:false,
        asNavFor: '.calendar-nav',
        infinite: false
        });

        $('.calendar-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.calendar-for',
        arrows: false,        
        swipe:false,
        focusOnSelect: false,
        infinite: false
        });
   }
  
}


        $(document).ready(function(){
                
        var disableScroll = false;
        var scrollPos = 0;
        function stopScroll() {
        disableScroll = true;
        scrollPos = $(window).scrollTop();
        }
        function enableScroll() {
        disableScroll = false;
        }
        
        $(function () {
            $(window).bind('scroll', function () {
                if (disableScroll) $(window).scrollTop(scrollPos);
            });
            $(window).bind('touchmove', function () {
                $(window).trigger('scroll');
            });
        });
        
        var widthW = $(window).width();
        
        gourmet.init();

        if(widthW > 767){
        
          $(".delivery-container").stick_in_parent({
          'offset_top' : 155
          });

          $(".filter-menu").stick_in_parent({
          'offset_top' : 175
          });

          $(".right-filter-menu").stick_in_parent({
          'offset_top' : 175
          });

          $('#data_search').on('keyup touchend',function(){
            var term = $('#data_search').val();
            var search_type = 'tag';
            var searchURL = '/search?type=product&q=' +search_type+':'+ term;
        
            if($(this).val() != '' ){
            $('.search-result-container').addClass('visible');
            $('body,html').addClass('overflow-hidden');
            }else{
            $('.top_header').removeClass('open-search');
            $('.search-result-container').removeClass('visible');
            $('body,html').removeClass('overflow-hidden');
            }        
            $.ajax({
              type: 'POST',
              url: searchURL,
              dataType: 'html',
              beforeSend:  function(){
                $('.loading-text').fadeIn();
                $('.loaded-heading').css('visibility','hidden');
                $('.search-section.section-padding .grid').css('visibility','hidden');
              	$('.no_result_search').hide();
              },
              error: function(jqXHR, textStatus, errorThrown) {
              	console.log(textStatus);
              },
              success: function(response) {
                $('.loading-text').fadeOut();
                $('.loaded-heading').css('visibility','visible');
                var product = $(response).find('.search-section.section-padding .grid').html();
                product = product.trim();
                if(product.length > 1){
                  $('.no_result_search').hide();
                  $('.search-result-container .grid .results_search').html(product);
                  $('.search-result-container .grid .results_search').css('visibility','visible');
                  gourmet.onLoadSelect();
                }
                else{
                  $('.no_result_search').show();
                  $('.search-result-container .grid .results_search').css('visibility','hidden');
                }
              }
            });
          });

          var currentUrl= $(location).attr('href');
          if(currentUrl.indexOf('#') != -1 ){
          var newScroll = $(window).scrollTop();
          	$(window).scrollTop(newScroll - 160);
          }
        }
        else{
        	$('#data_search-1').on(' keyup touchend',function(){
            var data = $('#data_search-1').val();
            data = data.replace(' ','+');
            if($(this).val() != '' ){
            $('.search-result-container').addClass('visible');
            $('body,html').addClass('overflow-hidden');
            }else{
            $('.top_header').removeClass('open-search');
            $('.search-result-container').removeClass('visible');
            $('body,html').removeClass('overflow-hidden');
            }        
            $.ajax({
              type: 'POST',
              url: '/search?type=product&q='+data,
              dataType: 'html',
              beforeSend:  function(){
                $('.loading-text').fadeIn();
                $('.loaded-heading').css('visibility','hidden');
                $('.search-section.section-padding .grid').css('visibility','hidden');
                $('.no_result_search').hide();
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
              },
              success: function(response) {
               $('.loading-text').fadeOut();
                $('.loaded-heading').css('visibility','visible');
                var product = $(response).find('.search-section.section-padding .grid').html();
                product = product.trim();
                if(product.length > 1){
                  $('.no_result_search').hide();
                  $('.search-result-container .grid .results_search').html(product);
                  $('.search-result-container .grid .results_search').css('visibility','visible');
                  gourmet.onLoadSelect();
                }
                else{
                  $('.no_result_search').show();
                  $('.search-result-container .grid .results_search').css('visibility','hidden');
                }
              }
            });
          });
        }
        
        });

        $(document).mouseup(function(e) 
        {        
          var delContainer = $(".switch-delivery,.delivery-drawer,.calendar-drawer,.close_verified");
          if (!delContainer.is(e.target) && delContainer.has(e.target).length === 0)
          {        
            $('body,html').removeClass('delivery-overflow');
            $('.delivery-overlay').fadeOut();
            $('.calendar-drawer-overlay').fadeOut();
            $('.delivery-drawer').removeClass('open');
            $('.calendar-drawer').removeClass('open');        
          }
          var cartContainer = $(".cart-switch,.cart-drawer");        
          if (!cartContainer.is(e.target) && cartContainer.has(e.target).length === 0)
          {        
            $('body,html').removeClass('cart-overflow');
            $('.cart-drawer-overlay').fadeOut();        
            $('.cart-drawer').removeClass('open');
          }
        });
        

        var scrollStop = function ( callback ) {
        if ( !callback || Object.prototype.toString.call( callback ) !== '[object Function]' ) return;
        var isScrolling;
        window.addEventListener('scroll', function ( event ) {
        window.clearTimeout( isScrolling );
        isScrolling = setTimeout(function() {
        callback();
        }, 66);
        }, false);
        };

        scrollStop(function () {        
          if($(".faq-category-list").length > 0){
            function filscrollToActive(){        
              var obj = $('.scroll-link.active');
              var childPos = obj.offset();
              var parentPos = obj.parents('.faq-category-list').offset();
              var childOffset = {
                top: childPos.top - parentPos.top,
                left: childPos.left - parentPos.left
              }
              var currentScroll = $(".faq-category-list").scrollLeft();
              $(".faq-category-list").animate({
              	scrollLeft: currentScroll + childOffset.left - 10
              },500);
            }
          filscrollToActive();
          }        
        });
       